<?php

namespace App\Http\Controllers;

use App\Company;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return 'hi';
        if(Auth::check()){
            $company=Company::where('user_id',Auth::user()->id)->get();
            return view('company.index')->with('company',$company);
        }
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()){
            $company = Company::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'user_id' => Auth::user()->id
            ]);
            if($company){
                return redirect()->route('company.show', ['company'=> $company->id])
                    ->with('success' , 'Company created successfully');
            }
        }
//        return 'hi';
        return back()->withInput()->with('error', 'Error creating new company');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $com=Company::find($company->id);
    
        $project=Project::all();
        $data=array(
            'company'=>$com,
        );
        return view('company.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        $com=Company::find($company->id);

        $project=Project::all();
        $data=array(
            'company'=>$com,
            'project'=>$project
        );
        return view('company.edit',$data);
    }
    public function change($id)
    {
        $com=Company::find($id);

        $project=Project::all();
        $data=array(
            'company'=>$com,
            'project'=>$project
        );
        return view('company.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
//        return $request->all();
        $companyUpdate=Company::where('id',$company->id)
                                ->update([
                                    'name'=>$request->input(['company-name']),
                                    'description'=>$request->input(['description']),
                                ]);
        if ($companyUpdate){
            return redirect()->route('company.show',['company'=>$company->id])
                             ->with('success','Company Updated Successfully!');
        }
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {    
        $findCompany=Company::find($company->id);    
        $project=Project::where('company_id',$company->id)->delete();        
        if($findCompany->delete()){                    
            return redirect()->route('company.index')
                ->with('success','Company deleted successfully!');
        }
        return back()->withInput()->with('error','Company could not be deleted!');
    }
    function delete($id){
        $findCompany=Company::find($id);    
        $project=Project::where('company_id',$id)->delete();        
        if($findCompany->delete()){                    
            return redirect()->route('company.index')
                ->with('success','Company deleted successfully!');
        }
        return back()->withInput()->with('error','Company could not be deleted!');
    }
}
