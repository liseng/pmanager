<?php

namespace App\Http\Controllers;

use App\Company;
use App\Project;
use App\ProjectUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $project=Project::where('user_id',Auth::user()->id)->get();
            return view('project.index')->with('project',$project);
        }
        return view('auth.login');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $company_id =null)
    {

        $company=null;
        if(!$company_id){

            $company=Company::where('user_id',Auth::user()->id)->get();
//            return $company;
        }
        return view('project.create',['company_id'=>$company_id,'company'=>$company]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check()){
            $project = Project::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'company_id' => $request->input('company_id'),
                'user_id' => Auth::user()->id
            ]);
            if($project){
                return redirect()->route('project.show', ['project'=> $project->id])
                    ->with('success' , 'Project created successfully');
            }
        }
//        return 'hi';
        return back()->withInput()->with('error', 'Error creating new company');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $pro=Project::find($project->id);
        $project=Project::all();
        $comment=$pro->comments;
        $data=array(
            'project'=>$pro,
            'comment'=>$comment,
        );
        return view('project.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $com=Project::find($project->id);

        $project=Project::all();
        $data=array(
            'company'=>$com,
            'project'=>$project
        );
        return view('project.edit',$data);
    }
    public function change($id)
    {
        $com=Project::find($id);

        $project=Project::all();
        $data=array(
            'company'=>$com,
            'project'=>$project
        );
        return view('project.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
//        return $request->all();
        $projectUpdate=Project::where('id',$project->id)
            ->update([
                'name'=>$request->input(['company-name']),
                'description'=>$request->input(['description']),
            ]);
        if ($projectUpdate){
            return redirect()->route('project.show',['company'=>$project->id])
                ->with('success','Project Updated Successfully!');
        }
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $findProject=Project::find($project->id);
        if($findProject->delete()){
            return redirect()->route('project.index')
                ->with('success','Project deleted successfully!');
        }
        return back()->withInput()->with('error','Project could not be deleted!');
    }
    function delete($id){
        $findProject=Project::find($id);
        if($findProject->delete()){
            return redirect()->route('project.index')
                ->with('success','Project deleted successfully!');
        }
        return back()->withInput()->with('error','Project could not be deleted!');
    }


    public function addUser(Request $request){
        
        $project=Project::find($request->project_id);        
        
        if(Auth::user()->id == $project->user_id){
            
            $user=User::where('email',$request->user_email)->first();
            //  $user=User::where('email','engliseng.it@gmail.com')->first();
            // dd($user);
            if($user){
                $projectUser=ProjectUser::where('user_id',$user->id)->where('project_id',$project->id)->first();
                if($projectUser){
                    return redirect()->route('project.show',['project'=>$project->id])
                        ->with('success',$request->user_email.' is already a member of this project!');
                }
            }
            
            if($project && $user){

                $project->users()->attach($user->id);
                return redirect()->route('project.show',['project'=>$project->id])
                    ->with('success',$request->user_email.' was added to the project Successfully!');
            }
        }
        return redirect()->route('project.show',['project'=>$project->id])
            ->with('error','Error adding user to the project!');

    }
}
