<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable=[
        "name",
        "description",
        "user_id",
    ];
    function user(){
        return $this->belongsTo('app\User');
    }
    public function projects(){
        return $this->hasMany('app\Project');
    }
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
}
