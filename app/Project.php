<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable=[
        "name",
        "description",
        "days",
        "user_id",
        "company_id",
    ];

    function users(){
        return $this->belongsToMany('app\User');
    }
    function company(){
        return $this->belongsTo('app\Company');
    }
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
}
