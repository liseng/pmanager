<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable=[
        "name",
        "project_id",
        "days",
        "hours",
        "user_id",
        "company_id",
    ];

    function user(){
        return $this->belongsTo('app\Model\User');
    }
    function project(){
        return $this->belongsTo('app\Model\projectModel');
    }
    function company(){
        return $this->belongsTo('app\Model\companyModel');
    }
    function users(){
        return $this->belongsToMany('app\Model\User');
    }
}
