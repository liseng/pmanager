<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth'])->group(function(){
    Route::resource('company', 'CompanyController');
    Route::get('/company/edit/{id}','CompanyController@change');
    Route::get('/company/delete/{id}','CompanyController@delete');

    Route::resource('project', 'ProjectController');
    Route::get('/project/create/{company_id?}','ProjectController@create');
    Route::get('/project/edit/{id}','ProjectController@change');
    Route::get('/project/delete/{id}','ProjectController@delete');
    

    Route::post('/project/adduser/{project_id?}','ProjectController@addUser')->name('project.adduser');
    Route::resource('role', 'RoleController');
    Route::resource('task', 'TaskController');
    Route::resource('user', 'UserController');
    Route::resource('comment', 'CommentController');
});


