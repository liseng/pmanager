<div class="row">
    <div class="col-md-12 col-sm-12  col-xs-12 col-lg-12">
        <div class="card border-primary">
            <div class="card-header" style="background-color: #2196F3;">
                Recent Comments
            </div>
            <div class="card-body">
                <ul class="media-list pl0">
                    @foreach($comment as $item)
                        <div class="media">
                            <img class="mr-3" src="http://placehold.it/60x60" alt="Generic placeholder image">
                            <div class="media-body">
                                <span class="float-right" style="font-size: 10px;">{{ $item->created_at->diffForHumans()}}</span>
                                <h5 class="mt-0"><a href="#">{{$item->user->email}}</a></h5>
                                {{ $item->body }}
                                <br>
                                <b>Proof: </b>
                                <a href="{{ $item->url }}" target="_blank">{{ $item->url }}</a>
                            </div>
                        </div>
                        <br>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
