@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{url('css/custom.css')}}">
@endsection
@section('content')
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">
                Create new company
            </div>
            <div class="card-body">
                <div class="col-md-12 add-form">
                    <form method="post" action="{{route('company.store')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name<span class="required">*</span></label>
                            <input class="form-control" type="text" placeholder="Enter Name" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Description</label>
                            <textarea required style="resize: vertical" spellcheck="false" class="form-control" id="project-content" name="description" rows="5"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
        <div class="sidebar-module">
            <h4>Actions</h4>
            <ol class="list-unstyled">
                <li><a href="/company">View Company</a></li>
            </ol>
        </div>
    </div>
@endsection