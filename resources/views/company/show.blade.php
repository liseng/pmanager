@extends('layouts.app')
@section('content')
    <div class="col-md-9">
        <div class="jumbotron">
            <div class="container">
                <h1 class="display-5">{{$company->name}}</h1>
                <p>{{$company->description}}</p>
                {{--<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p>--}}
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                Projects
                <a class="float-right" href="company/create"><span class="fa fa-plus" style="color: #000000;"></span></a>
            </div>
            <div class="card-body">
                @foreach($company->projects as $item)
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">{{$item->name}}</h3>
                            <p class="card-text">{{$item->description}}</p>
                            <a href="/project/{{$item->id}}" class="btn btn-primary">View Detail</a>
                        </div>
                    </div><br>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
        <div class="sidebar-module">
            <h4>Actions</h4>
            <ol class="list-unstyled">
                <li><a href="/company/{{$company->id}}/edit">Edit</a></li>
                <li><a href="/project/create/{{$company->id}}">Add project</a></li>
                <li><a href="/company">View companies</a></li>
                <li><a href="/company/create">Create new company</a></li>
                <hr>
                <li>
                    <a href="#" onclick="
                        var result = confirm('Are you sure you wish to delete this Company?');
                            if( result ){
                                  event.preventDefault();
                                  document.getElementById('delete-form').submit();
                            }
                        ">Delete
                    </a>
                    <form id="delete-form" action="{{ route('company.destroy',[$company->id]) }}"
                          method="POST" style="display: none;">
                        <input type="hidden" name="_method" value="delete">
                        {{ csrf_field() }}
                    </form>
                </li>
                {{--<li><a href="#">Add new user</a></li>--}}
            </ol>
        </div>
    </div>
@endsection