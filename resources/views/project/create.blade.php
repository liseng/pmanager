@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{url('css/custom.css')}}">
@endsection
@section('content')
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">
                Create new project
            </div>
            <div class="card-body">
                <div class="col-md-12 add-form">
                    <form method="post" action="{{route('project.store')}}">
                        {{csrf_field()}}
                        @if($company == null)
                            <input type="hidden" id="company" name="company_id" value="{{$company_id}}">
                        @endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name<span class="required">*</span></label>
                            <input class="form-control" type="text" placeholder="Enter Name" name="name" required>
                        </div>
                        @if($company != null)
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Example select</label>
                                <select class="form-control" id="exampleFormControlSelect1" name="company_id" required>
                                    <option value="" selected disabled>Choose a company</option>
                                    @foreach($company as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Description</label>
                            <textarea required style="resize: vertical" spellcheck="false" class="form-control" id="project-content" name="description" rows="5"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
        <div class="sidebar-module">
            <h4>Actions</h4>
            <ol class="list-unstyled">
                <li><a href="/project">View Project</a></li>
            </ol>
        </div>
    </div>
@endsection