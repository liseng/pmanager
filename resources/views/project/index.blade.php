@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
@endsection
@section('content')
    <div class="col-lg-8 col-md-8 offset-lg-2 offset-md-2">
        <div class="card">
            <div class="card-header">
                Project
                <a class="float-right" href="project/create"><span class="fa fa-plus" style="color: #000000;"></span></a>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Create Date</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($project as $item)
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td><a href="/project/{{$item->id}}">{{$item->name}}</a></td>
                            <td>{{$item->created_at->diffForHumans()}}</td>
                            <td class="text-center">
                                <a href="/project/edit/{{$item->id}}"><span class="fa fa-pencil-alt" ></span> Edit</a>
                                &nbsp;&nbsp;                    
                                <a href="#" onclick="Delete({{$item->id}})" ><span class="fa fa-trash"></span> Delete</a>
                            </td>  
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>    
            function Delete(company_id){        
                var result = confirm('Are you sure you wish to delete this Company?');
                if( result ){
                    event.preventDefault();
                    window.location.href="/project/delete/"+company_id;                                  
                }            
            }
        </script>
@endsection

