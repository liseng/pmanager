@extends('layouts.app')
@section('content')
    <div class="col-md-9">
        <div class="jumbotron">
            <div class="container">
                <h1 class="display-5">{{$project->name}}</h1>
                <p>{{$project->description}}</p>
            </div>
        </div>
        @include('partial.comment')
        <br>
        <form method="post" action="{{route('comment.store')}}">
            {{csrf_field()}}
            <input type="hidden" name="commentable_type" value="app\Project">
            <input type="hidden" name="commentable_id" value="{{$project->id}}">

            <div class="form-group">
                <label for="exampleFormControlTextarea1">Comment</label>
                <textarea style="resize: vertical" spellcheck="false" class="form-control" id="comment-content" name="body" rows="3" placeholder="Enter comment"></textarea>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Proof of work done (Url/Photos)</label>
                <textarea style="resize: vertical" spellcheck="false" class="form-control" id="comment-content" name="url" rows="2" placeholder="Enter url or screenshots"></textarea>
            </div>
            <button type="submit" class="btn btn-primary mb16">Submit</button>
        </form>
    </div>

    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
        <div class="sidebar-module">
            <h4>Actions</h4>
            <ol class="list-unstyled">
                <li><a href="/project/{{$project->id}}/edit">Edit</a></li>
                <li><a href="/project/create">Add project</a></li>
                <li><a href="/project">View projects</a></li>
                {{--<li><a href="/project/create">Create new project</a></li>--}}
                <hr>
                @if($project->user_id==Auth::user()->id)
                    <li>
                        <a href="#" onclick="
                            var result = confirm('Are you sure you wish to delete this project?');
                                if( result ){
                                      event.preventDefault();
                                      document.getElementById('delete-form').submit();
                                }
                            ">Delete
                        </a>

                        <form id="delete-form" action="{{ route('project.destroy',[$project->id]) }}"
                              method="POST" style="display: none;">
                            <input type="hidden" name="_method" value="delete">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif
                {{--<li><a href="#">Add new user</a></li>--}}
            </ol>
        </div>
        <br>
        <div class="sidebar-module">
            <h4>Add memeber</h4>
            <ol class="list-unstyled">
                <li>
                    <form id="add-user-form" action="{{ route('project.adduser') }}" method="POST">
                        <input type="hidden" id="project_id" name="project_id" value="{{$project->id}}">
                        {{ csrf_field() }}

                        <div class="input-group mb-3">
                            <input required name="user_email" type="email" class="form-control" placeholder="Enter email" aria-label="Add member" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Add</button>
                            </div>
                        </div>
                    </form>

                </li>
            </ol>
        </div>
        <br>
        <div class="sidebar-module">
            <h4>Members</h4>
            <ol class="list-unstyled">
                @foreach($project->users as $item)
                    <li>
                        <a href="#">{{$item->email}}</a>
                    </li>
                @endforeach

            </ol>
        </div>
    </div>
@endsection